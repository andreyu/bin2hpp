# Binary to C-array converter

> based on BIN2HPP.CPP (02.09.2000) by Shabarshin A. A.

### Download and build

You can view the source code repository on BitBucket or get a copy using git with the following command:
```sh
git clone https://bitbucket.org/andreyu/bin2hpp.git
cd bin2hpp
make release
```

### Usage

```sh
bin2hpp input_path [output_path] [options]
```
`output_path` and `options` are optional.

***

> *© 2000-2024 Andrey A. Ugolnik.*
> *All Rights Reserved.*
> *https://www.ugolnik.info*
> *andrey@ugolnik.info*
